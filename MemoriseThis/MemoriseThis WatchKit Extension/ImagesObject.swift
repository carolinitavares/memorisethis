//
//  ImagesObject.swift
//  MemoriseThis WatchKit Extension
//
//  Created by MacStudent on 2019-03-14.
//  Copyright © 2019 Lambton. All rights reserved.
//

import WatchKit

class ImagesObject: NSObject {

    var id: Int?
    var image: UIImage?
    
    // MARK: contructor
    convenience override init() {
        //let d = Date()
        self.init(id:0, image:UIImage())
    }
    
    init(id:Int, image: UIImage) {
        self.id = id
        self.image = image
    }
    
}
