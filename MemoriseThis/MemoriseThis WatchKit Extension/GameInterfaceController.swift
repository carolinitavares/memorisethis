//
//  GameInterfaceController.swift
//  MemoriseThis WatchKit Extension
//
//  Created by MacStudent on 2019-03-14.
//  Copyright © 2019 Lambton. All rights reserved.
//

import WatchKit
import Foundation


class GameInterfaceController: WKInterfaceController {

    @IBOutlet weak var signs: WKInterfaceImage!
    @IBOutlet weak var signsBtn: WKInterfaceButton!
    
    @IBOutlet weak var btn1: WKInterfaceButton!
    @IBOutlet weak var btn2: WKInterfaceButton!
    @IBOutlet weak var btn3: WKInterfaceButton!
    @IBOutlet weak var btn4: WKInterfaceButton!
    @IBOutlet weak var groupBtn: WKInterfaceGroup!
    
    @IBOutlet weak var groupSigns: WKInterfaceGroup!
    @IBOutlet weak var img1: WKInterfaceImage!
    @IBOutlet weak var img2: WKInterfaceImage!
    @IBOutlet weak var img3: WKInterfaceImage!
    @IBOutlet weak var img4: WKInterfaceImage!
    
    
    var ramdonImg:[ImagesObject/*UIImage*/] = []//(1..<5).shuffled()//:[String] = []
    var rightAnswer:[ImagesObject/*UIImage*/] = []
    var userAnswer:[Int] = []
    var ramdonImgShuffled:[ImagesObject] = []
    var isHard = false
    var nbo = 0
    
    var SLEEP_TIME:Double  = 5
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)

        let dict = context as? NSDictionary
        if dict != nil {
            let segue = dict!["segue"] as! String
            print(segue)
            
            if (segue == "hard") {
                print("hard")
                SLEEP_TIME = 0.9
                isHard = true
            }else {
                print("easy")
                SLEEP_TIME = 5
                isHard = false
            }
        }
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        self.createImg()
        
        self.play()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func play(){
        ramdonImgShuffled = ramdonImg.shuffled()
        rightAnswer = ramdonImg.shuffled()
        
        //print("right answer = \(rightAnswer)")
        
        signs.setHidden(true)
        groupBtn.setHidden(true)
        signsBtn.setHidden(true)
        
        groupSigns.setHidden(false)
        img1.setImage(rightAnswer[0].image)
        img2.setImage(rightAnswer[1].image)
        img3.setImage(rightAnswer[2].image)
        img4.setImage(rightAnswer[3].image)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + SLEEP_TIME) {
            self.groupSigns.setHidden(true)
            
            self.btn1.setBackgroundImage(self.ramdonImgShuffled[0].image)
            self.btn2.setBackgroundImage(self.ramdonImgShuffled[1].image)
            self.btn3.setBackgroundImage(self.ramdonImgShuffled[2].image)
            self.btn4.setBackgroundImage(self.ramdonImgShuffled[3].image)
            
            self.groupBtn.setHidden(false)
        }
        
        /*DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
         for(index, element) in self.rightAnswer.enumerated() {
         print("right answer [\(index)]= \(element.id as! Int)")
         //signsBtn.setBackgroundImage(element.image)
         
         
         self.signs.setImage(element.image)
         }
         }*/
        
        
        
        /*for (index, element) in ramdonImgShuffled.enumerated() {
         signsBtn.setBackgroundImage(element)
         //sleep(SLEEP_TIME)
         }*/
        //print("count -> \(ramdonImgShuffled.count)")
        
        
        
        //signsBtn.setHidden(true)
        //
        //print("ramdon = \(ramdonImgShuffled)")
        //groupBtn.setHidden(false)
        
        /*btn1.setTitle("\(ramdonImgShuffled[0].id as! Int)")
         btn2.setTitle("\(ramdonImgShuffled[1].id as! Int)")
         btn3.setTitle("\(ramdonImgShuffled[2].id as! Int)")
         btn4.setTitle("\(ramdonImgShuffled[3].id as! Int)")*/
    }
    
    func createImg(){
        let bacon = ImagesObject(id: 1, image: UIImage(named: "bacon")!)
        let burguer = ImagesObject(id: 2, image: UIImage(named: "burguer")!)
        let chicken = ImagesObject(id: 3, image: UIImage(named: "chicken")!)
        let fries = ImagesObject(id: 4, image: UIImage(named: "fries")!)
        
        ramdonImg.append(bacon)
        ramdonImg.append(burguer)
        ramdonImg.append(chicken)
        ramdonImg.append(fries)
        
        if isHard {
            //fase2()
        }
        nbo = ramdonImg.count
        
    }
    
    func fase2(){
        let pizza = ImagesObject(id: 5, image: UIImage(named: "pizza")!)
        ramdonImg.append(pizza)
    }
    
    func checkCorrect(){
        var compare:[Int] = []
        
        for (index, element) in rightAnswer.enumerated() {
            compare.append(element.id!)
        }
        
        print("right answer -> \(compare)")
        print("user answer \(userAnswer)")
        
        if (userAnswer == compare){
            print("same")
            
            signs.setImage(UIImage(named: "correct"))
            groupBtn.setHidden(true)
            groupSigns.setHidden(true)
            signs.setHidden(false)
            signsBtn.setHidden(false)
            
        }else{
            print("diferente")
            
            signs.setImage(UIImage(named: "incorrect"))
            groupBtn.setHidden(true)
            groupSigns.setHidden(true)
            signs.setHidden(false)
            signsBtn.setHidden(false)
        }
        
    }

    @IBAction func btn1Clicked() {
        print("btn 1 pressed")
        userAnswer.append(ramdonImgShuffled[0].id!)
        
        if(userAnswer.count == nbo){
            checkCorrect()
        }
    }
    @IBAction func btn2Clicked() {
        print("btn 2 pressed")
        userAnswer.append(ramdonImgShuffled[1].id!)
        
        if(userAnswer.count == nbo){
            checkCorrect()
        }
    }
    @IBAction func btn3Clicked() {
        print("btn 3 pressed")
        userAnswer.append(ramdonImgShuffled[2].id!)
        
        if(userAnswer.count == nbo){
            checkCorrect()
        }
    }
    @IBAction func btn4Clicked() {
        print("btn 4 pressed")
        userAnswer.append(ramdonImgShuffled[3].id!)
        
        if(userAnswer.count == nbo){
            checkCorrect()
        }
    }
    
    @IBAction func signsBtnClicked() {
        print("entrei")
        ramdonImgShuffled.removeAll()
        rightAnswer.removeAll()
        userAnswer.removeAll()
        
        play()
    }
    
    
}
