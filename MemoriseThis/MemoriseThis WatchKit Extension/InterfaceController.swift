//
//  InterfaceController.swift
//  MemoriseThis WatchKit Extension
//
//  Created by MacStudent on 2019-03-14.
//  Copyright © 2019 Lambton. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    
    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    @IBOutlet weak var group: WKInterfaceGroup!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        let suggestedResponses = ["Jenele", "Emad", "Carol", "Marcos"]

        
/* i will be here until 3:30  --=> probably in the game room.
 if you need to demo, come find me after you finish your test , or monday*/
        
        
        // 0. Check if there is a previously saved city
        let sharedPreferences = UserDefaults.standard
        var name = sharedPreferences.string(forKey: "name")
        
        if (name == nil) {
            // by default, the strating city is Vancouver
            group.setHidden(true)
            
            presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
                
                if (results != nil && results!.count > 0) {
                    // 2. write your code to process the person's response
                    let userResponse = results?.first as? String
                    self.nameLabel.setText("Hello \(userResponse as! String)!")
                    sharedPreferences.set(userResponse as! String, forKey: "name")
                    //self.popToRootController()
                    self.group.setHidden(false)
                }
            }

            
        }else{
            group.setHidden(false)
            nameLabel.setText("Hello \(name as! String)!")
        }

        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
        if segueIdentifier == "easy" {
            return ["segue" : "easy"]
        }else if segueIdentifier == "hard" {
            return ["segue" : "hard"]
        }else{
            return["segue": "error"]
        }
    }

}
